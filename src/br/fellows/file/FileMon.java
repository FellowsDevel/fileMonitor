package br.fellows.file;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.List;

public class FileMon {

    @SuppressWarnings( "rawtypes" )
    public static void main( String[] args ) throws Exception {
        ReadConfigJSON rcj = null;
        try {
            rcj = new ReadConfigJSON();
            rcj.parse();
        } catch( Exception e ) {
                        System.out.println( e.getMessage() );
            System.exit( 1 );
        }
        HashMap<String, FileConfig> configurations = rcj.getConfigs();
        WatchService watchService = FileSystems.getDefault().newWatchService();

        if( configurations == null ) {
            System.out.println( "Verifique a exist�ncia do arquivo de configura��o 'preferences.config'\n\n" );
            System.exit( 1 );
        }

        for( FileConfig fc : configurations.values() ) {
            Path folderSrc = Paths.get( fc.getFolderSrc() );
            configurations.put( fc.getSrcFileExtension(), fc );
            folderSrc.register( watchService, ENTRY_CREATE, ENTRY_MODIFY );
        }

        // boolean valid = true;
        // do {
        for( ;; ) {
            WatchKey watchKey = watchService.take();
            List<WatchEvent<?>> events = watchKey.pollEvents();

            for( WatchEvent watchEvent : events ) {

                WatchEvent.Kind kind = watchEvent.kind();

                if( kind == OVERFLOW ) {
                    System.out.println( "OVERFLOW!!!!" );
                    continue;
                }

                // Nome do evento que aconteceu.
                String eventKindName = kind.name();
                 System.out.println( "Nome do evento: " + eventKindName );

                // Pega o nome do arquivo que disparou o evento
                String fileName = watchEvent.context().toString();
                String extension = "";

                int i = fileName.lastIndexOf( "." );
                int p = Math.max( fileName.lastIndexOf( "/" ), fileName.lastIndexOf( "\\" ) );
                if( i > p ) {
                    extension = fileName.substring( i + 1 );
                } else {
                    continue;
                }
                // System.out.println( "Arquivo: " + fileName );
                // System.out.println( "pasta: " + watched );
                // System.out.println( " " );

                // Verifica se o arquivo est� na pasta de origem
                File f = new File( configurations.get( extension ).getFolderSrc() + fileName );
                if( !f.exists() && !f.isDirectory() ) {
                    // System.out.println( "O arquivo <" + fileName + "> n�o se encontra na pasta
                    // correta, ignorando..." );
                } else {
                    if( eventKindName.equals( "ENTRY_MODIFY" ) ) {
                        // executa a a��o
                        // System.out.println( "A c�pia do arquivo na pasta de origem terminou,
                        // executando a a��o configurada\n" );
                        doAction( configurations.get( extension ), fileName );
                    }
                }
            }
            if( !watchKey.reset() ) {
                break;
            }
        }
        watchService.close();
    }

    private static void doAction( FileConfig fc, String fileName ) {

        String dstExt = fc.getDstFileExtension();
        String dstFileWithNewExtension = fileName.substring( 0, fileName.lastIndexOf( "." ) ) + "." + dstExt;

        Path src = Paths.get( fc.getFolderSrc() + fileName );
        // Path dst = Paths.get( fc.getFolderDst() );
        Path dst = Paths.get( fc.getFolderDst() + dstFileWithNewExtension );

        switch( fc.getAction() ) {
            case ACTION_MOVE:
                while( true ) {
                    try {
                        // Files.move( src, dst.resolve( src.getFileName() ),
                        // StandardCopyOption.REPLACE_EXISTING );
                        Files.move( src, dst, StandardCopyOption.REPLACE_EXISTING );
                        break;
                    } catch( IOException e ) {
                        // e.printStackTrace();
                    }
                }
                System.out.println( "Arquivo <" + src.getFileName() + "> movido para <" + dst.getFileName() + ">..." );
                break;

            default:
                while( true ) {
                    try {
                        // Files.copy( src, dst.resolve( src.getFileName() ),
                        // StandardCopyOption.REPLACE_EXISTING );
                        Files.copy( src, dst, StandardCopyOption.REPLACE_EXISTING );
                        break;
                    } catch( IOException e ) {
                        // e.printStackTrace();
                    }
                }
                System.out.println( "Arquivo <" + src.getFileName() + "> copiado para <" + dst.getFileName() + ">..." );
                break;
        }
    }
}
