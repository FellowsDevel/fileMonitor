package br.fellows.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import br.fellows.file.exceptions.DiretorioNaoExisteException;

/**
 * Esta classe manipula o acesso ao arquivo de configura��o em formato JSON
 * 
 * Os diret�rios ser�o verificados se existem. Se n�o existirem ser� lan�ada exce��o
 * DiretorioNaoExisteException
 * 
 * Se as configura��es de cria��o de diret�rio n�o est�o marcadas para cria��o o diret�rio n�o
 * existir
 * ser� levantada uma exce��o informando que o diret�rio n�o existe e a execu��o ser� interrompida.
 * 
 * @author andre
 *
 */
public class ReadConfigJSON {

    private HashMap<String, FileConfig> hm;

    public ReadConfigJSON() {
        hm = new HashMap<>();
    }

    /**
     * Inicia a leitura do arquivo de configura��o com as configura��es
     * de cria��o de pastas, se previamente halbilitadas.
     */
    public void parse() throws Exception {
        try {
            init();
        } catch( FileNotFoundException e ) {
            throw new Exception( "Arquivo de configura��o n�o encontrado\n" + e.getMessage() );
        } catch( IOException e ) {
            throw new Exception( e.getMessage() );
        } catch( ParseException e ) {
            throw new Exception( "Favor verificar o arquivo de configura��o\n" + e.getMessage() );
        } catch( DiretorioNaoExisteException e ) {
            throw new Exception( e.getMessage() );
        }
    }

    public HashMap<String, FileConfig> getConfigs() {
        return this.hm;
    }

    /**
     * Se o diret�rio informado n�o existir, ser� lan�ada uma exce��o
     * 
     * @param dirname
     * @throws Exception
     */
    private void checkDir( String dirname ) throws DiretorioNaoExisteException {
        File theDir = new File( dirname );

        if( !theDir.exists() ) {
            throw new DiretorioNaoExisteException( dirname );
        }
    }

    /**
     * Se o diret�rio informado n�o existir, o mesmo ser� criado
     * 
     * @param dirname
     */
    private void createDir( String dirname ) {
        File theDir = new File( dirname );

        if( !theDir.exists() ) {
            try {
                Files.createDirectories( Paths.get( dirname ) );
            } catch( IOException e ) {
                e.printStackTrace();
            }
        }
    }

    private void init() throws Exception {

        JSONParser parser = new JSONParser();
        FileReader fr = new FileReader( "./preferences.config" );

        JSONArray fileConfs = (JSONArray) parser.parse( fr );
        for( Object confs : fileConfs ) {
            JSONObject conf = (JSONObject) confs;

            String srcFileExtension = (String) conf.get( "SrcFileExtension" );
            String dstFileExtension = (String) conf.get( "DstFileExtension" );
            String folderSrc = (String) conf.get( "FolderSrc" );
            String folderDst = (String) conf.get( "FolderDst" );
            String sCreateSrcFolder = (String) conf.get( "CreateSrcFolder" );
            String sCreateDstFolder = (String) conf.get( "CreateDstFolder" );
            boolean createSrcFolder = false;
            boolean createDstFolder = false;
            String action = (String) conf.get( "Action" );

            if( sCreateSrcFolder != null ) {
                createSrcFolder = Boolean.parseBoolean( sCreateSrcFolder );
            } else {
                createSrcFolder = false;
            }

            if( sCreateDstFolder != null ) {
                createDstFolder = Boolean.parseBoolean( sCreateDstFolder );
            } else {
                createDstFolder = false;
            }

            if( srcFileExtension == null ) {
                System.out.println( "Tag 'FileExtension' n�o encontrada. Verifique suas configura��es" );
                System.exit( 1 );
            }

            if( dstFileExtension == null ) {
                dstFileExtension = srcFileExtension;
            }

            if( folderSrc == null ) {
                System.out.println( "Tag 'FolderSrc' n�o encontrada. Verifique suas configura��es" );
                System.exit( 1 );
            }
            if( folderDst == null ) {
                System.out.println( "Tag 'FolderDst' n�o encontrada. Verifique suas configura��es" );
                System.exit( 1 );
            }

            if( !folderSrc.endsWith( "/" ) ) {
                folderSrc += "/";
            }

            if( !folderDst.endsWith( "/" ) ) {
                folderDst += "/";
            }

            FileConfig.Action acao;
            if( action != null && action.equalsIgnoreCase( "move" ) ) {
                acao = FileConfig.Action.ACTION_MOVE;
                System.out.print( "Os arquivos de extens�o <" + srcFileExtension + "> ser�o MOVIDOS" );
                if( dstFileExtension != null && dstFileExtension != srcFileExtension ) {
                    System.out.println( " e renomeados para a extens�o <" + dstFileExtension + ">" );
                } else {
                    System.out.println( "." );
                }
            } else {
                acao = FileConfig.Action.ACTION_COPY;
                System.out.print( "Os arquivos de extens�o <" + srcFileExtension + "> ser�o COPIADOS e SUBSTITU�DOS na pasta destino" );
                if( dstFileExtension != null && dstFileExtension != srcFileExtension ) {
                    System.out.println( " e renomeados para a extens�o <" + dstFileExtension + ">" );
                } else {
                    System.out.println( "." );
                }
            }
            System.out.println( "Pasta origem: <" + folderSrc + ">. Pasta destino: <" + folderDst + ">\n" );

            if( createSrcFolder ) {
                createDir( folderSrc );
            }

            if( createDstFolder ) {
                createDir( folderDst );
            }

            checkDir( folderSrc );
            checkDir( folderDst );

            FileConfig fc = new FileConfig();
            fc.setSrcFileExtension( srcFileExtension );
            fc.setDstFileExtension( dstFileExtension );
            fc.setCreateDstFolder( createDstFolder );
            fc.setCreateSrcFolder( createSrcFolder );
            fc.setFolderDst( folderDst );
            fc.setFolderSrc( folderSrc );
            fc.setAction( acao );

            hm.put( srcFileExtension, fc );

            // System.out.println( "File name to match: " + fc.getFileName() );
            // System.out.println( "Folder Origin: " + fc.getFolderOrigin() );
            // System.out.println( "Folder Destination: " + fc.getFolderDest() );
            //
            // System.out.println( "" );

        }
    }
}
