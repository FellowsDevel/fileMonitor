package br.fellows.file;

public class FileConfig {

    public enum Action {
        ACTION_COPY( 0x01 ), ACTION_MOVE( 0x02 );

        private final int value;

        Action( int value ) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private String  srcFileExtension;
    private String  dstFileExtension;
    private String  folderSrc;
    private String  folderDst;
    private boolean createSrcFolder;
    private boolean createDstFolder;
    private Action  action;

    public FileConfig() {
    }

    public void setAction( Action action ) {
        this.action = action;
    }

    public Action getAction() {
        return this.action;
    }

    public void setSrcFileExtension( String srcFileExtension ) {
        this.srcFileExtension = srcFileExtension;
    }

    public String getSrcFileExtension() {
        return this.srcFileExtension;
    }

    public String getDstFileExtension() {
        return dstFileExtension;
    }

    public void setDstFileExtension( String dstFileExtension ) {
        this.dstFileExtension = dstFileExtension;
    }

    public String getFolderSrc() {
        return folderSrc;
    }

    public void setFolderSrc( String folderSrc ) {
        this.folderSrc = folderSrc;
    }

    public String getFolderDst() {
        return folderDst;
    }

    public void setFolderDst( String folderDst ) {
        this.folderDst = folderDst;
    }

    public boolean isCreateSrcFolder() {
        return createSrcFolder;
    }

    public void setCreateSrcFolder( boolean createSrcFolder ) {
        this.createSrcFolder = createSrcFolder;
    }

    public boolean isCreateDstFolder() {
        return createDstFolder;
    }

    public void setCreateDstFolder( boolean createDstFolder ) {
        this.createDstFolder = createDstFolder;
    }

}
